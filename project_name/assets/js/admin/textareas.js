jQuery(function() {
    jQuery('textarea:not([id*=__prefix__]):not([id*=rawcontent]):not([name^=meta]):not([name^=_])')
        .livequery(function() {
                jQuery(this).wysihtml5({
                	"font-styles": false,
                	"emphasis": true,
                	"lists": true,
                	"html": true,
                	"link": true,
                	"image": false
                })
        })
})