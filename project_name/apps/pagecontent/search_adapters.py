import watson
from django.template.defaultfilters import strip_tags

class BaseCMSAdapter(watson.SearchAdapter):
    fieldnames = ('text', 'title', 'richtext')

    def get_title(self, obj):
        return obj.title

    def _index_content(self, obj):
        s = ' '.join([getattr(content,fieldname) \
            for region in obj.template.regions_dict \
            for content in getattr(obj.content,region) \
            for fieldname in self.fieldnames if hasattr(content,fieldname)])

        return strip_tags(s)

class PageSearchAdapter(BaseCMSAdapter):

    def get_description(self, obj):
        return ''

    def get_content(self, obj):
        content = super(PageSearchAdapter, self).get_content(obj)
        content += self._index_content(obj)
        content += self._index_parent(obj)
        return content

    def _index_parent(self, obj):
        ret = ''
        p = obj.get_root()
        if p:
            ret += ('%s %s') % (p.title, p.slug)
        return ret

class EntrySearchAdapter(BaseCMSAdapter):

    def get_description(self, obj):
        try:
            ret = strip_tags(obj.blogrichtextcontent_set.all()[0].text)
        except:
            ret = ''
        return ret

    def get_content(self, obj):
        content = super(EntrySearchAdapter, self).get_content(obj)
        content += self._index_content(obj)
        content += self._index_tags(obj)
        content += self._index_categories(obj)
        return content

    def _index_categories(self, obj):
        ret = ''
        for c in obj.categories.all():
            t = c.get_translation()
            ret += ' '+t.slug
        return ret

    def _index_tags(self, obj):
        return ' '.join(obj.tags.values_list('slug', flat=True))
