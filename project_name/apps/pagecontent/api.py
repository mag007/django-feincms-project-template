from tastypie.resources import ModelResource
from tastypie.authorization import Authorization
from tastypie import fields

from tastypie.constants import ALL, ALL_WITH_RELATIONS
from elephantblog.models import Entry


class BlogEntryResource(ModelResource):
    content_main = fields.CharField()
    
    class Meta:
        authorization = Authorization()
        detail_allowed_methods = ['get']
        always_return_data = True
        
        list_allowed_methods = ['get']
        detail_allowed_methods = ['get']
        
        queryset = Entry.objects.filter(is_active=True)
        resource_name = 'blog_entry_resource'
        
        excludes = ['is_active']
        
        filtering = {
            'id':ALL
        }
    
    def dehydrate_content_main(self, bundle):
        return ''.join([i.render() for i in bundle.obj.content.main])