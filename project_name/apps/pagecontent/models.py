from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from feincms.module.page.models import Page
from feincms.module.page import processors

# Import our page templates
from page_templates import TEMPLATES

# Import our FeinCMS content types
from feincms.content.richtext.models import RichTextContent
from feincms.content.raw.models import RawContent
from feincms.content.section.models import SectionContent
from feincms.content.medialibrary.v2 import MediaFileContent
from feincms.content.application.models import ApplicationContent

# Import ElephantBlog Entry
from elephantblog.models import Entry

Entry.register_extensions('tags')
Entry.register_regions(
        ('main', _('Main content area')),
    )
Entry.create_content_type(RichTextContent, cleanse=False, regions=('main',))
Entry.create_content_type(MediaFileContent, TYPE_CHOICES=(
        ('default', _('default')),
        ('right', _('Right')),
        ('left', _('Left')),
    ))

Page.register_extensions('changedate', 'translations', 'navigation', \
    'seo', 'symlinks', 'titles', 'sites', 'ct_tracker')

Page.register_templates(*TEMPLATES)

Page.create_content_type(RichTextContent)
Page.create_content_type(SectionContent, TYPE_CHOICES=(
    ('full',_('Full Width')),
    ('half',_('Half Width')),
    ))
Page.create_content_type(MediaFileContent, TYPE_CHOICES=(
    ('default', _('default')),
    ('lightbox', _('lightbox')),
    ))
Page.create_content_type(ApplicationContent, APPLICATIONS=(
        ('elephantblog.urls', _('Blog')),
    ))

Page.create_content_type(RawContent)

Page.register_request_processor(processors.frontendediting_request_processor)
Page.register_with_reversion()

# Register our Watson search adapters
import watson
from search_adapters import PageSearchAdapter, EntrySearchAdapter
watson.register(Page.objects.filter(active=True), PageSearchAdapter)
watson.register(Entry.objects.filter(is_active=True), EntrySearchAdapter)