TEMPLATES = (

        {
        	"title": "Homepage",
        	"path": "pages/homepage.html",
        	"regions": [
        		["main", "Main"],
        	]
        },		

		{
			"title": "Single Column",
			"path": "pages/single_column.html",
			"regions": [
				["main", "Main"],
			]
		},

		{
			"title": "Blog",
			"path": "pages/blog.html",
			"regions": [
				["main", "Main"],
				["sidebar", "Sidebar"],
			]
		},

)