from django.contrib import admin
from django.conf.urls.defaults import patterns, include, url
from django.http import HttpResponse
from tastypie.api import Api
from django.conf import settings
import os

# TastyPie REST API
v1_api = Api(api_name='v1')
for resource in ():
    v1_api.register(resource())


# See: https://docs.djangoproject.com/en/dev/ref/contrib/admin/#hooking-adminsite-instances-into-your-urlconf
admin.autodiscover()


# See: https://docs.djangoproject.com/en/dev/topics/http/urls/
urlpatterns = patterns('',
    # Admin panel and documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(v1_api.urls)),
    url(r'', include('feincms.urls')),
)

# Static Serve hack + Favicon Icon
urlpatterns = patterns('',
    (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATICFILES_DIRS[0]}),
    (r'^favicon\.ico$', 'django.views.generic.simple.redirect_to', {'url': os.path.join(settings.STATIC_URL, 'favicon.ico')}),
) + urlpatterns


# Blitz Authorization, See: https://devcenter.heroku.com/articles/blitz
if not settings.DEBUG:
    def confirm(request):
        return HttpResponse(42)
    
    urlpatterns += patterns('', url(r'^mu-115235a9-cc71139e-b0f1f267-7b167a18.txt', confirm))


# Serve Media Files
if settings.DEBUG:
    urlpatterns = patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        url(r'', include('django.contrib.staticfiles.urls')),
    ) + urlpatterns