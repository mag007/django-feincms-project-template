"""Management utilities."""


from fabric.contrib.console import confirm
from fabric.api import abort, env, local, settings, task, prompt


########## GLOBALS
env.run = 'heroku run python manage.py'
env.site_name = '{{ project_name }}'
HEROKU_ADDONS = (
    'heroku-postgresql:dev',
    'memcache:5mb',
    'newrelic:standard',
    'pgbackups:auto-month',
    'sentry:developer',
)
HEROKU_CONFIGS = (
    ('DJANGO_SETTINGS_MODULE', '%s.settings.prod' % env.site_name),
    ('BUILDPACK_URL', 'git://github.com/jiaaro/heroku-buildpack-django.git'),
)
HEROKU_MIGRATION_ORDER = (
    'medialibrary',
    'page',
)
########## END GLOBALS


########## HELPERS
def cont(cmd, message):
    """Given a command, ``cmd``, and a message, ``message``, allow a user to
    either continue or break execution if errors occur while executing ``cmd``.

    :param str cmd: The command to execute on the local system.
    :param str message: The message to display to the user on failure.

    .. note::
        ``message`` should be phrased in the form of a question, as if ``cmd``'s
        execution fails, we'll ask the user to press 'y' or 'n' to continue or
        cancel exeuction, respectively.

    Usage::

        cont('heroku run ...', "Couldn't complete %s. Continue anyway?" % cmd)
    """
    with settings(warn_only=True):
        result = local(cmd, capture=True)

    if message and result.failed and not confirm(message):
        abort('Stopped execution per user request.')
########## END HELPERS

########## INSTALLATION MANAGEMENT
@task
def install():
    # Do some basic stuff...
    local("python manage.py syncdb")
    local("python manage.py schemamigration page --initial")
    local("python manage.py schemamigration medialibrary --initial")
    local("python manage.py schemamigration elephantblog --initial")
    local("python manage.py migrate")
    # Load our fixtures...
    local("python manage.py loaddata medialibrary page elephantblog")
    # We like git...
    local("git init")
    # and Twitter Bootstrap,
    local("git submodule add git://github.com/twitter/bootstrap.git %s/submodules/bootstrap" % (env.site_name))
    # Set HEAD to a specific commit in a new branch
    local('(cd %s/submodules/bootstrap/; git checkout -b 9f29785783e58a7ffc6f8779c2ae55edebd54ba2)' % env.site_name)
    # and ipython.
    local("pip install ipython")
    # Also install a non-borked readline
    local("easy_install readline")
    # Do our first commit
    #local("git add -A")
    #local("git commit -m 'Initial commit'")
########## INSTALLATION MANAGEMENT

########## DEVELOPMENT MANAGEMENT
@task
def migratesouth(app):
    # haha
    local('python manage.py schemamigration %s --auto' % app)
    local('python manage.py migrate page')
########## DEVELOPMENT MANAGEMENT

########## DATABASE MANAGEMENT
@task
def syncdb():
    """Run a syncdb."""
    local('%(run)s syncdb --noinput' % env)


@task
def migrate(app=None):
    """Apply one (or more) migrations. If no app is specified, fabric will
    attempt to run a site-wide migration.

    :param str app: Django app name to migrate.
    """
    if app:
        local('%s migrate %s --noinput' % (env.run, app))
    else:
        local('%(run)s migrate --noinput' % env)

@task
def loaddata(*fixtures):
    local('%s loaddata %s' % (env.run, ''.join(fixtures)))
########## END DATABASE MANAGEMENT


########## FILE MANAGEMENT
@task
def collectstatic():
    """Collect all static files, and copy them to S3 for production usage."""
    local('%(run)s collectstatic --noinput' % env)

@task
def compress():
    """Compress files with Django Compressor"""
    local('%(run)s compress' % env)
########## END FILE MANAGEMENT


########## HEROKU MANAGEMENT
@task
def bootstrap(app=''):
    """Bootstrap your new application with Heroku, preparing it for a production
    deployment. This will:

        - Create a new Heroku application.
        - Install all ``HEROKU_ADDONS``.
        - Sync the database.
        - Apply all database migrations.
        - Initialize New Relic's monitoring add-on.
    """
    cont('heroku create %s' % app, "Couldn't create the Heroku app, continue anyway?")

    for addon in HEROKU_ADDONS:
        cont('heroku addons:add %s' % addon,
            "Couldn't add %s to your Heroku app, continue anyway?" % addon)

    ### Configure everthing ###
    config()
    amazon_s3_config()

    ### Push to Heroku ###
    cont('git push heroku master',
            "Couldn't push your application to Heroku, continue anyway?")

    ### Do our database commands ###
    syncdb()

    for app in HEROKU_MIGRATION_ORDER:
        cont('%s migrate %s' % (env.run, app),
            "Couldn't migrate %s, continue anyways?" % app)

    migrate()

    prompt('Specify fixtures to load: ', 'fixtures', default=None)
    if env.fixtures:
        loaddata(env.fixtures)

    ### Do our file management commands ###
    collectstatic()
    compress()

    cont('%(run)s newrelic-admin validate-config - stdout' % env,
            "Couldn't initialize New Relic, continue anyway?")


@task
def config():
    """
    Add all key/value pairs from HEROKU_CONFIGS
    """
    cont('heroku config:add %s' % ' '.join(['='.join(config) for config in HEROKU_CONFIGS]),
            "Couldn't set the configs for your Heroku app, continue anyway?")


@task
def amazon_s3_config():
    """
    Configure Amazon S3
    """
    prompt('Specify AWS_ACCESS_KEY_ID: ', 'AWS_ACCESS_KEY_ID', default=None)
    prompt('Specify AWS_SECRET_ACCESS_KEY: ', 'AWS_SECRET_ACCESS_KEY', default=None)
    prompt('Specify AWS_STORAGE_BUCKET_NAME: ', 'AWS_STORAGE_BUCKET_NAME', default=None)

    if env.AWS_ACCESS_KEY_ID and env.AWS_SECRET_ACCESS_KEY and env.AWS_STORAGE_BUCKET_NAME:
        cont('heroku config:add AWS_ACCESS_KEY_ID=%s AWS_SECRET_ACCESS_KEY=%s AWS_STORAGE_BUCKET_NAME=%s' \
                % (env.AWS_ACCESS_KEY_ID, env.AWS_SECRET_ACCESS_KEY, env.AWS_STORAGE_BUCKET_NAME),
                "Couldn't set your Amazon S3 configs, continue anyway?")


@task
def destroy(app):
    """Destroy this Heroku application. Wipe it from existance.

    .. note::
        This really will completely destroy your application. Think twice.
    """
    local('heroku apps:destroy --app=%s' % app)
########## END HEROKU MANAGEMENT
